SET PATH=C:\Program Files\NASM;%PATH%
CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64

powershell.exe -Command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile('https://github.com/openssl/openssl/archive/%1.zip', '%1.zip')"
powershell.exe -Command "Expand-Archive -Path %1.zip -DestinationPath ."
DEL %1.zip

CD "openssl-%1"
perl.exe Configure VC-WIN64A
nmake.exe
MKDIR C:\OpenSSL
xcopy.exe /E include C:\OpenSSL\include\
COPY libcrypto-*.dll C:\OpenSSL\
COPY libcrypto.lib C:\OpenSSL\
COPY libssl-*.dll C:\OpenSSL\
COPY libssl.lib C:\OpenSSL\
CD ..
RMDIR /S /Q "openssl-%1"
