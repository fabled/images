#!/bin/sh
set -o xtrace -o nounset -o errexit -o pipefail

# initial pacman setup
sudo pacman-key --init
sudo pacman-key --populate
sudo reflector --country "US" --protocol https,http --score 20 --sort rate --save /etc/pacman.d/mirrorlist

# fetch package database and do full system upgrade
sudo pacman --noconfirm -Syu
